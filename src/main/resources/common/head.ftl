<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta content="IE=edge" http-equiv="X-UA-Compatible">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="${context}/css/bootstrap.min.css" media="all" type="text/css" />
  <link rel="stylesheet" href="${context}/css/bootstrap-glyphicon.css" media="all" type="text/css" />
  <link rel="stylesheet" href="${context}/css/fileinput.min.css" media="all" type="text/css" />
  <link href="${context}/css/styles.css" rel="stylesheet">  
  <title>Conversion Audio et Vid&eacute;o</title>
</head>
<body>
  <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="${context}/api/html">Conversion Audio et Vid&eacute;o</a>
      </div>
      <!-- <div class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
          <li><a href="${context}/api/html/jobs">Jobs</a></li>
          <li><a href="${context}/api/html/events">Events</a></li>
        </ul>
      </div> -->
    </div>
  </div>

  <div class="container">
	<#if msg_success??> 
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			${msg_success}
		</div>
	</#if>
	<#if msg_error??> 
		<div class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			${msg_error}
		</div>
	</#if>

  		