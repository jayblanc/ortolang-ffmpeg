<#include "/common/head.ftl">

<div class="starter-template">
	<div class="container">
		<div class="row">
			<section>
	        <div class="wizard">
	            <div class="wizard-inner">
	                <div class="connecting-line"></div>
	                <ul class="nav nav-tabs" role="tablist">
	                    <li role="presentation" class="active">
	                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Choisir les fichiers à convertir">
	                            <span class="round-tab">
	                                <i class="glyphicon glyphicon-folder-open"></i>
	                            </span>
	                        </a>
	                    </li>
	                    <li role="presentation" class="disabled">
	                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Définir les paramètres de conversion">
	                            <span class="round-tab">
	                                <i class="glyphicon glyphicon-pencil"></i>
	                            </span>
	                        </a>
	                    </li>
	                    <li role="presentation" class="disabled">
	                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Visualiser un échantillon">
	                            <span class="round-tab">
	                                <i class="glyphicon glyphicon-picture"></i>
	                            </span>
	                        </a>
	                    </li>
	                    <li role="presentation" class="disabled">
	                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Démarrer la conversion">
	                            <span class="round-tab">
	                                <i class="glyphicon glyphicon-ok"></i>
	                            </span>
	                        </a>
	                    </li>
	                </ul>
	            </div>
	
	            <form role="form">
	                <div class="tab-content">
	                    <div class="tab-pane active step1" role="tabpanel" id="step1">
	                    	<h4>Choisir les fichiers à convertir</h4>
	                    	<p>Les fichiers seront téléchargés sur le serveur pour pouvoir être convertis.</p>
	                        <div class="row">
	                            <div class="col-md-12">
		                    		<input id="input-id" name="input-file" type="file" class="file" multiple data-show-caption="true">
								</div>
							</div>
	                        <div class="row">
	                            <div class="col-md-6">
		                    		<button type="button" class="btn btn-primary next-step">Continuer</button>
		                    	</div>
		                    </div>
		                </div>
	                    <div class="tab-pane" role="tabpanel" id="step2">
	                        <h4>Définir les paramètres de conversion</h4>
	                    	<p>Les paramètres de conversion peuvent être fixés fichier par fichier, un aperçu pourra être générée avant la conversion finale.</p>
	                        <div class="row">
	                            <div class="col-md-6">
		                    	    <button type="button" class="btn btn-default prev-step">Revenir</button>
	                            	<button type="button" class="btn btn-primary next-step">Sauvegarder et continuer</button>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="tab-pane" role="tabpanel" id="step3">
	                        <h3>Visualiser un aperçu de la conversion</h3>
	                        <p>Si l'aperçu ne vous convient pas, il est possible de revenir en arrière afin de modifier les paramètres de conversion</p>
	                        <div class="row">
	                            <div class="col-md-6">
	                            	<button type="button" class="btn btn-default prev-step">Previous</button>
	                            	<button type="button" class="btn btn-default next-step">Skip</button>
	                            	<button type="button" class="btn btn-primary btn-info-full next-step">Save and continue</button>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="tab-pane" role="tabpanel" id="complete">
	                        <h3>Complete</h3>
	                        <p>You have successfully completed all steps.</p>
	                    </div>
	                    <div class="clearfix"></div>
	                </div>
	            </form>
	        </div>
	    	</section>
		</div>
	</div>
</div>

<#include "/common/foot.ftl">  
