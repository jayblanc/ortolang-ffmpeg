package fr.ortolang.tools;

import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.mvc.freemarker.FreemarkerMvcFeature;


public class ToolApplication extends ResourceConfig {

    public ToolApplication() {
        packages("fr.ortolang.tools.resource");
        register(LoggingFilter.class);
        register(FreemarkerMvcFeature.class);
    }
}