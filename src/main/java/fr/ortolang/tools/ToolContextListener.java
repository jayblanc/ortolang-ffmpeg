package fr.ortolang.tools;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import fr.ortolang.tools.manager.JobManager;

public class ToolContextListener implements ServletContextListener {
    
    private static Logger LOGGER = Logger.getLogger(ToolContextListener.class.getName());

    @Override
    public void contextInitialized(ServletContextEvent ctx) {
        LOGGER.log(Level.INFO, "Context Initilized");
        ToolConfig.getInstance();
        JobManager.start();
    }
    
    @Override
    public void contextDestroyed(ServletContextEvent ctx) {
        JobManager.stop();
        LOGGER.log(Level.INFO, "Context Destroyed");
    }

}

