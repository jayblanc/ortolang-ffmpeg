package fr.ortolang.tools.workspace;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.ortolang.tools.ToolConfig;

public class WorkspaceManagerImpl extends WorkspaceManager {

    private static final Logger LOGGER = Logger.getLogger(WorkspaceManagerImpl.class.getName());

    private Path root;
    
    protected WorkspaceManagerImpl() {
        LOGGER.log(Level.INFO, "Creating new WorkspaceManager");
        root = Paths.get(ToolConfig.getInstance().getProperty(ToolConfig.Property.WORKSPACES_HOME));
        LOGGER.log(Level.INFO, "JobManager built at root path : " + root);
    }

    @Override
    void init() {
        LOGGER.log(Level.INFO, "Initialising WorkspaceManager");
        if (!Files.exists(root)) {
            try {
                Files.createDirectories(root);
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "unable to initilize workspaces home", e);
            }
        }
        LOGGER.log(Level.INFO, "WorkspaceManager initialized with home: " + root);
    }

    @Override
    Path getWorkspace(String userid, String jobid) {
        LOGGER.log(Level.INFO, "Getting workspace for user: " + userid);
        Path ws = Paths.get(root.toString(), userid, jobid);
        if ( !Files.exists(ws) ) {
            LOGGER.log(Level.INFO, "Creating new directory for workspace");
            try {
                Files.createDirectories(ws);
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "unable to create workspace", e);
            }
        }
        Path wssys = Paths.get(ws.toString(), ".ws");
        if ( !Files.exists(wssys) ) {
            try {
                LOGGER.log(Level.INFO, "Creating system folder for workspace");
                Files.createDirectories(wssys);
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "unable to create workspace", e);
            }
            Path infos = Paths.get(wssys.toString(), "infos.properties");
            try ( OutputStream os = Files.newOutputStream(infos, StandardOpenOption.CREATE_NEW) ) {
                Properties props = new Properties();
                props.setProperty("creation", Long.toString(System.currentTimeMillis()) );
                props.store(os, "DO NOT EDIT MANUALLY, created by Video Convertion Tool");
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "unable to create workspace", e);
            }
        }
        return ws;
    }

    @Override
    void purgeWorkspace(String userid, String jobid) {
        LOGGER.log(Level.INFO, "Purge workspace for user: " + userid);
        Path ws = Paths.get(root.toString(), userid, jobid);
        try {
            Files.walkFileTree(ws, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }
                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }

            });
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "unable to purge workspace", e);
        }
    }

}
