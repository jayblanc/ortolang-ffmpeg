package fr.ortolang.tools.workspace;

import java.nio.file.Path;


public abstract class WorkspaceManager {

    private static class WorkspaceManagerHolder {
        private final static WorkspaceManagerImpl instance = new WorkspaceManagerImpl();
    }

    public static WorkspaceManager getInstance() {
        return WorkspaceManagerHolder.instance;
    }

    abstract void init();
    
    abstract Path getWorkspace(String userid, String jobid);
    
    abstract void purgeWorkspace(String userid, String jobid);

}
