package fr.ortolang.tools;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ToolConfig {

    private static final String TOOL_NAME = "ffmpeg";
    private static final Logger LOGGER = Logger.getLogger(ToolConfig.class.getName());
    private static ToolConfig config;
    private Properties props;
    private Path home;

    private ToolConfig() throws Exception {
        if ( System.getenv("ORTOLANG_TOOLS_HOME") != null ) {
            home = Paths.get(System.getenv("ORTOLANG_TOOLS_HOME"), TOOL_NAME);
        } else if ( System.getProperty("ORTOLANG_TOOLS_HOME") != null ) {
            home = Paths.get(System.getProperty("ORTOLANG_TOOLS_HOME"), TOOL_NAME);
        } else {
            home = Paths.get(System.getProperty("user.home"), ".ortolang/tools", TOOL_NAME);
        }
        if ( !Files.exists(home) ) {
            Files.createDirectories(home);
        }
        LOGGER.log(Level.INFO, "TOOL_HOME set to : " + home);

        props = new Properties();
        init();
    }

    public static synchronized ToolConfig getInstance() {
        if (config == null) {
            try {
                config = new ToolConfig();
            } catch (Exception e) {
                LOGGER.log(Level.SEVERE, "unable to load configuration", e);
                throw new RuntimeException("unable to load configuration", e);
            }
        }
        return config;
    }
    
    public Path getHomePath() {
        return home;
    }

    public String getProperty(ToolConfig.Property property) {
        return props.getProperty(property.key());
    }
    
    public void setProperty(ToolConfig.Property property, String value) throws IOException {
        props.setProperty(property.key(), value);
        save();
    }
    
    public void setProperties(Map<ToolConfig.Property, String> properties) throws IOException {
        for ( Entry<ToolConfig.Property, String> entry : properties.entrySet() ) {
            props.setProperty(entry.getKey().key(), entry.getValue());
        }
        save();
    }
    
    public Properties listProperties() {
        return props;
    }
    
    private void init() throws IOException {
        LOGGER.log(Level.INFO, "Initializing configuration");
        Path configFilePath = Paths.get(home.toString(), "config.properties");
        if ( !Files.exists(configFilePath) ) {
            LOGGER.log(Level.FINE, "configuration file does not exists, copying from default one");
            Files.copy(ToolConfig.class.getClassLoader().getResourceAsStream("config.properties"), configFilePath);
        }
        load();
    }
    
    private void load() throws IOException {
        LOGGER.log(Level.FINE, "loading configuration from filesystem");
        Path configFilePath = Paths.get(home.toString(), "config.properties");
        try (InputStream in = Files.newInputStream(configFilePath) ) {
            props.load(in);
        }
        LOGGER.log(Level.FINE, "configuration loaded");
    }
    
    private void save() throws IOException {
        LOGGER.log(Level.FINE, "saving configuration to filesytem");
        Path configFilePath = Paths.get(home.toString(), "config.properties");
        try (OutputStream out = Files.newOutputStream(configFilePath) ) {
            props.store(out, "AUTO GENERATED FILE, DO NOT MODIFY MANUALLY");
        }
        LOGGER.log(Level.FINE, "configuration saved");
    }

    public enum Property {

        WORKSPACES_HOME ("tool.workspaces.home"),
        WORKER_POOL_SIZE ("tool.worker.poolsize"),
        QUEUE_SIZE ("tool.queue.size"),
        QUOTA_JOBS ("tool.quota.jobs");
        
        private final String key;

        private Property(String name) {
            this.key = name;
        }

        public String key() {
            return key;
        }

    }

}
