package fr.ortolang.tools.resource;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.server.mvc.Template;

@Path("/html")
public class HtmlResource {

    @Context
    private ServletContext ctx;

    @GET
    @Template(name = "/home.ftl")
    @Produces(MediaType.TEXT_HTML)
    public Map<String, Object> root() {
        Map<String, Object> model = new HashMap<String, Object>();
        model.put("context", ctx.getContextPath());
        return model;
    }

}
