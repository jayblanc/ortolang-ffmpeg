package fr.ortolang.tools.resource;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;

@Path("/workspaces")
@Produces({ MediaType.APPLICATION_JSON })
public class WorkspaceResource {
    
    private static final Logger LOGGER = Logger.getLogger(WorkspaceResource.class.getName());

    @Context
    private ServletContext ctx;

    @POST
    @Path("/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response upload(FormDataMultiPart form, @Context Request request) throws UnsupportedEncodingException {
        LOGGER.log(Level.INFO, "POST /upload");
        for (Entry<String, List<FormDataBodyPart>> part : form.getFields().entrySet()) {
            LOGGER.log(Level.INFO, "found part: " + part.getKey() + ", size: " + part.getValue().size());
        }
        return Response.ok("{}").build();
    }
    
}
