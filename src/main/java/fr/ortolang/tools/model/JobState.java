package fr.ortolang.tools.model;

public enum JobState {
    
    PENDING, RUNNING, ABORTED, COMPLETED

}
