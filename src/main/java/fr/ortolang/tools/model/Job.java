package fr.ortolang.tools.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Version;

@Entity
@NamedQueries(value= {
        @NamedQuery(name="findJobsByState", query="select j from Job j where j.state = :state"),
        @NamedQuery(name="findJobsByOwner", query="select j from Job j where j.owner = :owner"),
        @NamedQuery(name="countPendingJobsForOwner", query="select count(j) from Job j where j.owner = :owner and ( j.state = 'PENDING' or j.state = 'RUNNING' )")
        })
public class Job implements Serializable {

    private static final long serialVersionUID = 6553092098728063910L;
    
    @Id
    private String id;
    @Version
    private long version;
    private String executor;
    private String owner;
    @Enumerated(EnumType.STRING)
    private JobState state;
    private long creation;
    private long start;
    private long stop;
    private long ttl;
    @ElementCollection
    private Map<String, String> parameters = new HashMap<String, String>();

    public Job() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExecutor() {
        return executor;
    }

    public void setExecutor(String executor) {
        this.executor = executor;
    }

    public JobState getState() {
        return state;
    }

    public void setState(JobState state) {
        this.state = state;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public String getParameter(String key) {
        return parameters.get(key);
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public long getCreation() {
        return creation;
    }

    public void setCreation(long creation) {
        this.creation = creation;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getStop() {
        return stop;
    }

    public void setStop(long stop) {
        this.stop = stop;
    }

    public long getTtl() {
        return ttl;
    }

    public void setTtl(long ttl) {
        this.ttl = ttl;
    }

}
