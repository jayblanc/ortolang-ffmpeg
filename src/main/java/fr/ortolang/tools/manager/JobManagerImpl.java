package fr.ortolang.tools.manager;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import fr.ortolang.tools.ToolConfig;
import fr.ortolang.tools.model.Job;
import fr.ortolang.tools.model.JobState;

public class JobManagerImpl extends JobManager {

    private static final Logger LOGGER = Logger.getLogger(JobManagerImpl.class.getName());

    private String connectionUrl;
    private String shutdownUrl;
    private ExecutorService executor;
    private EntityManagerFactory emf;
    private EntityManager em;
    
    protected JobManagerImpl() {
        LOGGER.log(Level.INFO, "Creating new JobMannager");
        try {
            LOGGER.log(Level.INFO, "Starting database");
            System.setProperty("derby.system.home", ToolConfig.getInstance().getHomePath().toString());
            shutdownUrl = "jdbc:derby:tooldb;shutdown=true";
            connectionUrl = "jdbc:derby:tooldb;create=true";
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
            DriverManager.getConnection(connectionUrl).close();
            LOGGER.log(Level.INFO, "Database started");
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Unable to start database", e);
        }
        try {
            LOGGER.log(Level.INFO, "Building Hibernate EntityManager");
            Map<String, String> props = new HashMap<String, String>();
            props.put("hibernate.connection.url", connectionUrl);
            emf = Persistence.createEntityManagerFactory("tool-pu");
            em = emf.createEntityManager();
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Unable to build hibernate EntityManager", e);

        }
        int nbthreads = Integer.parseInt(ToolConfig.getInstance().getProperty(ToolConfig.Property.WORKER_POOL_SIZE));
        executor = Executors.newFixedThreadPool(nbthreads);
        LOGGER.log(Level.INFO, "JobManager built with " + nbthreads + " threads in pool");
    }

    @Override
    public void postConstruct() {
        LOGGER.log(Level.INFO, "Starting Job Manager...");
        TypedQuery<Job> rquery = em.createNamedQuery("findJobsByState", Job.class).setParameter("state", JobState.RUNNING);
        List<Job> rjobs = rquery.getResultList();
        for (Job job : rjobs) {
            JobHandler handler = new JobHandler(this, job.getId());
            executor.execute(handler);       
        }
        LOGGER.log(Level.INFO, rjobs.size() + " jobs was running during shutdown: " + ((rjobs.size()>0)?"all restared":"nothing to do") );
        TypedQuery<Job> pquery = em.createNamedQuery("findJobsByState", Job.class).setParameter("state", JobState.PENDING);
        List<Job> pjobs = pquery.getResultList();
        for (Job job : pjobs) {
            JobHandler handler = new JobHandler(this, job.getId());
            executor.execute(handler);
        }
        LOGGER.log(Level.INFO, pjobs.size() + " pendings jobs: " + ((pjobs.size()>0)?"all queued":"nothing to do") );
        LOGGER.log(Level.INFO, "Job Manager started");
    }

    @Override
    public void preDestroy() {
        LOGGER.log(Level.INFO, "Stopping Job Manager...");
        LOGGER.log(Level.INFO, "Waiting for active workers to finish (timeout is 1 min)...");
        executor.shutdown();
        try {
            if (!executor.awaitTermination(60, TimeUnit.SECONDS)) {
                executor.shutdownNow(); 
                if (!executor.awaitTermination(60, TimeUnit.SECONDS)) {
                    LOGGER.log(Level.WARNING, "Executor stop timeout reached, some jobs might remain in a inconsistent state");
                }
            }
        } catch (InterruptedException ie) {
            executor.shutdownNow();
            Thread.currentThread().interrupt();
        }
        LOGGER.log(Level.INFO, "Stopping Persistence...");
        if (em != null) {
            em.close();
        }
        if (emf != null) {
            emf.close();
        }
        LOGGER.log(Level.INFO, "Persistence stopped...");
        LOGGER.log(Level.INFO, "Stopping Database.");
        try {
            DriverManager.getConnection(shutdownUrl).close();
            LOGGER.log(Level.INFO, "Database stopped");
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "Database shutdown problem", e);
        }
        LOGGER.log(Level.INFO, "Job Manager stopped");
    }

    @Override
    public String submit(String owner, Map<String, String> parameters) throws JobManagerException, QuotaExceededException {
        String id = UUID.randomUUID().toString();
        try {
            Job job = new Job();
            job.setId(id);
            job.setOwner(owner);
            job.setCreation(System.currentTimeMillis());
            job.setExecutor("ffmpeg");
            job.setState(JobState.PENDING);
            job.setTtl(-1);
            job.setParameters(parameters);
            Integer.parseInt(ToolConfig.getInstance().getProperty(ToolConfig.Property.QUOTA_JOBS));
            em.getTransaction().begin();
            int jobsquota = Integer.parseInt(ToolConfig.getInstance().getProperty(ToolConfig.Property.QUOTA_JOBS));
            if (jobsquota > 0) {
                TypedQuery<Integer> query = em.createNamedQuery("countPendingJobsForOwner", Integer.class).setParameter("owner", owner);
                Integer nbjobs = query.getSingleResult();
                if (nbjobs >= jobsquota) {
                    throw new QuotaExceededException("maximum job per user reached, please try again later");
                }
            }
            em.persist(job);
            em.getTransaction().commit();
            JobHandler handler = new JobHandler(this, id);
            executor.execute(handler);
            return id;
        } catch (QuotaExceededException e) {
            em.getTransaction().rollback();
            throw e;
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw new JobManagerException("unable to submit new job", e);
        }

    }

    @Override
    public Job read(String id) throws JobManagerException, JobNotFoundException {
        Job job = em.find(Job.class, id);
        if (job == null) {
            throw new JobNotFoundException("No job found in storage for id: " + id);
        }
        return job;
    }

    @Override
    public List<Job> list(String owner) throws JobManagerException {
        TypedQuery<Job> query = em.createNamedQuery("findJobsByOwner", Job.class).setParameter("owner", owner);
        List<Job> jobs = query.getResultList();
        return jobs;
    }

}
