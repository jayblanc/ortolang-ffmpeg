package fr.ortolang.tools.manager;

public class QuotaExceededException extends Exception {

    private static final long serialVersionUID = 2086275311339119483L;

    public QuotaExceededException() {
    }

    public QuotaExceededException(String message) {
        super(message);
    }

    public QuotaExceededException(Throwable cause) {
        super(cause);
    }

    public QuotaExceededException(String message, Throwable cause) {
        super(message, cause);
    }

    public QuotaExceededException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
