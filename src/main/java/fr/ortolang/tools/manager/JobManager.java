package fr.ortolang.tools.manager;

import java.util.List;
import java.util.Map;

import fr.ortolang.tools.model.Job;

public abstract class JobManager {
    
    private static class JobManagerHolder {
        private final static JobManagerImpl instance = new JobManagerImpl();
    }

    private static JobManager getInstance() {
        return JobManagerHolder.instance;
    }
    
    public static void start() {
        getInstance().postConstruct();
    }
    
    public static void stop() {
        getInstance().preDestroy();
    }

    abstract void postConstruct();
    
    abstract void preDestroy();
    
    abstract String submit(String owner, Map<String, String> parameters) throws JobManagerException, QuotaExceededException;
    
    abstract Job read(String id) throws JobManagerException, JobNotFoundException;
    
    abstract List<Job> list(String owner) throws JobManagerException;

}
