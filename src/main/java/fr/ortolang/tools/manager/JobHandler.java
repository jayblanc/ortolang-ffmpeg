package fr.ortolang.tools.manager;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import fr.ortolang.tools.model.Job;

public class JobHandler implements Runnable {

    private static final Logger LOGGER = Logger.getLogger(JobManagerImpl.class.getName());

    private final JobManager manager;
    private final String jobid;

    public JobHandler(JobManager manager, String jobid) {
        this.manager = manager;
        this.jobid = jobid;
    }

    @Override
    public void run() {
        LOGGER.log(Level.INFO, "Starting handling job with id: " + jobid);
        try {
            Job job = manager.read(jobid);
            LOGGER.log(Level.FINE, "Job loaded, need to start executor: " + job.getExecutor());
            Process p = Runtime.getRuntime().exec("ffmpeg -i ");
            p.waitFor();
        } catch (JobManagerException | JobNotFoundException | IOException | InterruptedException e) {
            e.printStackTrace();
        }
        LOGGER.log(Level.INFO, "Job " + jobid + " executed succesfully");
    }

}