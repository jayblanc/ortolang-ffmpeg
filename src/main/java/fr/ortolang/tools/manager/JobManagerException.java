package fr.ortolang.tools.manager;

public class JobManagerException extends Exception {

    private static final long serialVersionUID = -5911257099653842989L;

    public JobManagerException() {
    }

    public JobManagerException(String message) {
        super(message);
    }

    public JobManagerException(Throwable cause) {
        super(cause);
    }

    public JobManagerException(String message, Throwable cause) {
        super(message, cause);
    }

    public JobManagerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
